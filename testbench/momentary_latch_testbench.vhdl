library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

---------------------------------------------------------------------------
entity momentary_latch_testbench is
---------------------------------------------------------------------------
end entity;


---------------------------------------------------------------------------
architecture momentary_latch_testbench_1 of momentary_latch_testbench is
---------------------------------------------------------------------------
    constant clk_period : time := 1 ns;
    constant delay_bits : natural := 4;
    signal clk          : std_logic := '0';
    signal toggle       : std_logic := '1';
    signal output       : std_logic;
begin
    momentary_latch_inst : entity work.momentary_latch generic map(delay_bits => delay_bits)
        port map(toggle, clk, output);

    clk_proc : process
    begin
        clk <= '0';
        wait for clk_period / 2;
        clk <= '1';
        wait for clk_period / 2;
    end process;

    sim_proc : process
    begin
        wait for 5*clk_period;
        assert output = '0' report "output should initialize to 0";

        toggle <= '0';
        wait for clk_period;
        assert output = '0' report "output should not change immediately after toggle";
        wait for clk_period * 2**(delay_bits-1);
        assert output = '0' report "output should not change yet";

        toggle <= '1';
        wait for clk_period * 2**(delay_bits);
        assert output = '0' report "output should not change while toggle high";

        toggle <= '0';
        wait for clk_period * 2**(delay_bits+2);
        assert output = '1' report "output should change after delay";

        report "end";
        wait;
    end process;
end architecture momentary_latch_testbench_1;
