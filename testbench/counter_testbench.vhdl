library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

---------------------------------------------------------------------------
entity counter_testbench is
---------------------------------------------------------------------------
end entity;

---------------------------------------------------------------------------
architecture counter_testbench_1 of counter_testbench is
---------------------------------------------------------------------------
    constant n                       : natural := 4;
    constant clk_period              : time := 1 ns;
    signal enable, clk, clear, carry : std_logic := '0';
    signal count                     : natural range 0 to 2**n - 1;
begin
    counter_inst : entity work.counter generic map(n => n)
        port map(enable, clk, clear, count, carry);

    clk_proc : process
    begin
        clk <= '0';
        wait for clk_period / 2;
        clk <= '1';
        wait for clk_period / 2;
    end process;

    sim_proc : process
    begin
        enable <= '1';
        clear <= '1';
        wait for 2*clk_period;
        assert count = 0;
        assert carry = '0';

        clear <= '0';
        wait for clk_period;
        assert count = 1;

        enable <= '0';
        wait for 10*clk_period;
        assert count = 1 report "count shouldn't increment when enable is 0";
        enable <= '1';

        wait for 5*clk_period;
        assert count = 6 report "count should increase every cycle after enable is 1";

        clear <= '1';
        wait for 2 * clk_period;
        assert count = 0 report "count should be zero when clear is 1";

        clear <= '0';
        -- Test overflow
        wait for clk_period * (2**n - 1) + clk_period;
        assert count = 0 report "count should be zero after overflow";
        assert carry = '1' report "carry should be 1 after overflow";

        wait for 2 * clk_period;
        clear <= '1';
        wait for 2 * clk_period;
        clear <= '0';
        assert carry = '0' report "clear should reset carry";

        report "end";
        wait;
    end process;
end architecture counter_testbench_1;
