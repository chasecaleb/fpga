library ieee;
use ieee.std_logic_1164.all;

entity d_ff_testbench is
end d_ff_testbench;

architecture behavior of d_ff_testbench is
    signal d, clk, enable : std_logic := '0';
    signal q, q_not       : std_logic;
    constant clk_period   : time := 1 ns;
begin
    d_ff_inst : entity work.d_ff port map(d, clk, enable, q, q_not);

    clk_proc : process
    begin
        clk <= '0';
        wait for clk_period / 2;
        clk <= '1';
        wait for clk_period / 2;
    end process;

    sim_proc : process
    begin
        d <= '1';
        wait for 5*clk_period;
        assert q = '0' report "q should not change while enable is 0";
        assert q_not = '1';

        enable <= '1';
        wait for clk_period;
        assert q = '1' report "q should be 1 once enabled";
        assert q_not = '0';

        d <= '0';
        wait for clk_period;
        assert q = '0';
        assert q_not = '1';

        report "end";
        wait;
    end process;
end behavior;
