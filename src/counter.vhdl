library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

---------------------------------------------------------------------------
entity counter is
---------------------------------------------------------------------------
generic(n : natural := 4);
port
(
    enable : in std_logic;
    clk  : in std_logic;
    clear  : in std_logic;
    count  : out natural range 0 to 2**n - 1 := 0;
    carry  : out std_logic := '0'
);
end entity;

---------------------------------------------------------------------------
architecture behavior of counter is
---------------------------------------------------------------------------
    signal pre_count : natural range 0 to 2**n - 1;
begin
    process(enable, clk, clear)
    begin
        if clear = '1' then
            pre_count <= 0;
            carry <= '0';
        elsif rising_edge(clk) and enable = '1' then
            if pre_count = 2**(n - 1) then
                pre_count <= 0;
                carry <= '1';
            else
                pre_count <= pre_count + 1;
            end if;
        end if;
    end process;

    count <= pre_count;
end architecture behavior;
