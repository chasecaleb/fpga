library ieee;
use ieee.std_logic_1164.all;

entity d_ff is
    port(
        d      : in std_logic;
        clk    : in std_logic;
        enable : in std_logic := '1';
        q      : out std_logic := '0';
        q_not  : out std_logic := '1');
end d_ff;

architecture behavior of d_ff is
    Signal q_next : std_logic := '0';
begin
    process(clk)
    begin
        if rising_edge(clk) and enable = '1' then
            q_next <= d;
        end if;
    end process;

    q <= q_next;
    q_not <= not(q_next);
end behavior;
