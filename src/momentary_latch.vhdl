library ieee;
use ieee.std_logic_1164.all;

-- Explanation/reference:
-- https://www.eewiki.net/display/LOGIC/Debounce+Logic+Circuit+(with+VHDL+example)
entity momentary_latch is
generic (delay_bits : natural := 20);
port
(
    toggle : in std_logic;
    clk    : in std_logic;
    output : out std_logic := '0'
);
end momentary_latch;

architecture behavior of momentary_latch is
    signal toggle_not, toggle_change, ff_1_q, ff_2_q, ff_3_q, latch_q_not : std_logic;
    signal dly_overflow   : std_logic := '1';
    signal dly_enable : std_logic := '1';
begin
    ff_1 : entity work.d_ff port map(d => toggle_not, clk => clk, q => ff_1_q);
    ff_2 : entity work.d_ff port map(d => ff_1_q, clk => clk, q => ff_2_q);
    ff_3 : entity work.d_ff port map(d => ff_2_q, clk => clk, enable =>
        dly_overflow, q => ff_3_q);
    latch : entity work.d_ff port map(d => latch_q_not, clk => ff_3_q, q => output, q_not => latch_q_not);
    dly  : entity work.counter generic map(n => delay_bits)
        port map(enable => dly_enable, clk => clk, clear => toggle_change,
            carry => dly_overflow);

    toggle_not <= not(toggle);
    toggle_change <= ff_1_q xor ff_2_q;

end behavior;
