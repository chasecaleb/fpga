XILINX_PROJECT = ./fpga-xilinx
FLASH_TOOL = ./flash-tool/mimasconfig.py
FLASH_PORT = /dev/ttyACM0
SOURCES = src/d_ff.vhdl src/counter.vhdl src/momentary_latch.vhdl
TESTS = $(wildcard testbench/*.vhdl)
SIM_TIME = 250ns
TEST_NAMES = $(basename $(notdir $(TESTS)))
OBJECTS = $(patsubst %.vhdl, %.o, $(SOURCES) $(TESTS))

RED_OUTPUT = $(shell tput setaf 1)
RESET_OUTPUT = $(shell tput sgr0)

all: setup $(OBJECTS)

setup:
	@mkdir -p build

$(OBJECTS): setup
	ghdl -s --workdir=build $(patsubst %.o, %.vhdl, $@)
	ghdl -a --workdir=build $(patsubst %.o, %.vhdl, $@)

$(TEST_NAMES): $(OBJECTS) setup
	ghdl -e --workdir=build $@
	@mv $@ build/

tests: $(TEST_NAMES)
	@echo ""
	@echo "$(RED_OUTPUT)#####TESTS#####$(RESET_OUTPUT)"
	@$(foreach test, $(TEST_NAMES),\
		echo $(RED_OUTPUT)Running $(test):$(RESET_OUTPUT);\
		./build/$(test) --stop-time=$(SIM_TIME) --vcd=./build/$(test).vcd;)

# xilinx: all
xilinx:
	# cd $(XILINX_PROJECT) && xtclsh $(XILINX_PROJECT).tcl rebuild_project
	cd $(XILINX_PROJECT) && xtclsh $(XILINX_PROJECT).tcl run_process

flash: xilinx
	python $(FLASH_TOOL) $(FLASH_PORT) $(XILINX_PROJECT)/*.bin

clean:
	rm -rf build
